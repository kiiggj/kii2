<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Requests\GameRequest;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function store(GameRequest $request)
    {
        $answer = Answer::create($request->validated());
        return $answer;
    }
}
