<?php

namespace App\Http\Controllers;

use App\MeasureDanger;
use Illuminate\Http\Request;

class MeasureDangerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\MeasureDanger $measureDanger
     * @return \Illuminate\Http\Response
     */
    public function show(MeasureDanger $measureDanger)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\MeasureDanger $measureDanger
     * @return \Illuminate\Http\Response
     */
    public function edit(MeasureDanger $measureDanger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\MeasureDanger $measureDanger
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MeasureDanger $measureDanger)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\MeasureDanger $measureDanger
     * @return \Illuminate\Http\Response
     */
    public function destroy(MeasureDanger $measureDanger)
    {
        //
    }
}
