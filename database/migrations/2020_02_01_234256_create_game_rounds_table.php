<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('round_number');


            $table->integer('game_id')->unsigned();
            $table ->foreign('game_id')
                ->references('id')->on('games')
                ->onDelete('cascade');

            $table->integer('round_id')->unsigned();
            $table ->foreign('round_id')
                ->references('id')->on('rounds')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_rounds');
    }
}
