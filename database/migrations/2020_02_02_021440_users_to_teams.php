<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersToTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'game_id'))
        {
            Schema::table('users', function (Blueprint $table){

                $table->integer('game_id')->unsigned()->nullable();
                $table ->foreign('game_id')
                    ->references('id')->on('games');

            });
        }
        Schema::table('users', function (Blueprint $table){
           $table->boolean('isPlayer')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'isPlayer')){
            Schema::table('users', function (Blueprint $table){
                $table->dropColumn('isPlayer');
            });
        }

        Schema::table('users', function (Blueprint $table){
//            $table->dropForeign('game_id');
//            $table->dropColumn('isPlayer');
//            $table->dropColumn('game_id');
        });
    }
}
