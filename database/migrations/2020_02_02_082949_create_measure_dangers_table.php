<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasureDangersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_dangers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('measure_id')->unsigned();
            $table->boolean('raund_1');
            $table->boolean('raund_2');
            $table->boolean('raund_3');
            $table->integer('game_id')->unsigned();

            $table->foreign('measure_id')
                ->references('id')->on('measures')
                ->onDelete('cascade');

            $table->foreign('game_id')
                ->references('id')->on('games')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_dangers');
    }
}
