<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundDangersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('round_dangers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('danger_id')->unsigned();
            $table ->foreign('danger_id')
                ->references('id')->on('dangers')
                ->onDelete('cascade');

            $table->integer('round_id')->unsigned();
            $table ->foreign('round_id')
                ->references('id')->on('rounds')
                ->onDelete('cascade');

            $table->integer('measure_id')->unsigned();
            $table ->foreign('measure_id')
                ->references('id')->on('measures')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('round_dangers');
    }
}
